module.exports = {
  '*.{js,json,md,yml}': (filenames) => `prettier --write ${filenames.join(' ')} --ignore-path .gitignore`,
  '*.{css,js}': (filenames) => `stylelint --fix ${filenames.join(' ')} --ignore-path .gitignore`,
  '*.{js,js.njk,json}': (filenames) => `eslint --fix ${filenames.join(' ')} --ignore-path .gitignore`,
};
