module.exports = {
  // Remove any unused classes for the smallest file size
  purge: {
    mode: 'all',
    content: ['_site/**/*.html'],
    options: {
      whitelist: [],
    },
  },
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {},
    },
  },
  corePlugins: {
    position: true,
    overflow: true,
  },
  variants: {},
};
