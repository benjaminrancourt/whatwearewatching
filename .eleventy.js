const eleventyPluginJavaScript = require('@sherby/eleventy-plugin-javascript');
const eleventyPluginFilesMinifier = require('@sherby/eleventy-plugin-files-minifier');

const series = require('./data/series.json');
const status = require('./data/status.json');

module.exports = (eleventyConfig) => {
  eleventyConfig.addPlugin(eleventyPluginJavaScript);
  eleventyConfig.addPlugin(eleventyPluginFilesMinifier);

  // Disable automatic use of your .gitignore
  eleventyConfig.setUseGitIgnore(false);

  // Merge data instead of overriding
  eleventyConfig.setDataDeepMerge(true);

  // Add Tailwind Output CSS as Watch Target
  eleventyConfig.addWatchTarget('./_tmp/style.css');
  eleventyConfig.addWatchTarget('./data/series.json');
  eleventyConfig.addWatchTarget('./data/status.json');
  eleventyConfig.addWatchTarget('./src/admin/config.yml');

  // Copy Static Files to /_Site
  eleventyConfig.addPassthroughCopy({
    './_tmp/style.css': './static/css/style.css',
    './src/admin/config.yml': './admin/config.yml',
  });

  // Copy Image Folder to /_site
  eleventyConfig.addPassthroughCopy('./src/static/img');

  // Copy favicon to route of /_site
  eleventyConfig.addPassthroughCopy('./src/favicon.ico');

  eleventyConfig.setBrowserSyncConfig({
    notify: true,
  });

  // Set valid JavaScript delimiters
  eleventyConfig.setFrontMatterParsingOptions({
    delimiters: ['/*---', '---*/'],
  });

  // Add collections from JSON files
  eleventyConfig.addCollection('status', () => status);
  eleventyConfig.addCollection('series', () => series);

  // Let Eleventy transform HTML files as nunjucks
  // So that we can use .html instead of .njk
  return {
    dir: {
      input: 'src',
    },

    htmlTemplateEngine: 'njk',

    templateFormats: ['njk', 'html'],

    pathPrefix: '/',
  };
};
