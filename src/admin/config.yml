backend:
  # Use netlify identity as backend
  name: git-gateway
  # Branch to update
  branch: main

media_folder: 'src/static/img'
public_folder: '/static/img'

# Please run "npx netlify-cms-proxy-server" for local backend
local_backend: true

collections:
  - label: 'Series'

    # Allows users to create new items in the collection
    create: true

    # Unique identifier for the collection, used as the key when referenced in other contexts
    name: 'serie'
    folder: 'data/series'

    extension: 'json'

    editor:
      preview: false

    # An optional list of sort fields to show in the UI
    sortable_fields: ['title', 'status']

    # An optional list of predefined view groups to show in the UI
    view_groups:
      - label: Status
        field: status

    # Summary of the item in the UI
    summary: '{{title}}'

    fields:
      - hint: 'Title of the serie'
        label: 'Title'
        name: 'title'
        required: true
        widget: 'string'

      - collection: 'status'
        hint: 'Watching status for Ben & Mari'
        label: 'Status'
        name: 'status'
        required: true
        search_fields: ['title']
        value_field: '{{slug}}'
        widget: 'relation'

      - default: 'https://en.wikipedia.org/wiki/'
        hint: 'Wikipedia page, for more information on the serie'
        label: 'Wikipedia'
        name: 'wikipedia'
        pattern: ['https:\/\/en\.wikipedia\.org\/wiki\/.*', 'Must by a English Wikipedia page']
        required: true
        widget: 'string'

      - hint: 'Card image'
        label: 'Card'
        name: 'card'
        required: true
        widget: 'image'

      - default: 1
        hint: 'The last watched season number (or the current if we are watching it)'
        label: 'Current season number'
        min: 1
        name: 'currentSeasonNumber'
        required: true
        value_type: 'int'
        widget: 'number'

      - default: 1
        hint: 'Number of seasons of the serie'
        label: 'Number of seasons'
        min: 1
        name: 'numberOfSeasons'
        required: true
        value_type: 'int'
        widget: 'number'

      - hint: 'Information to help us remember when to check for a new season'
        label: 'Next season date'
        name: 'nextSeasonDate'
        required: false
        widget: 'string'

  - label: 'Status'
    # Unique identifier for the collection, used as the key when referenced in other contexts
    name: 'status'

    folder: 'data/status'
    extension: 'json'

    # Allows users to create new items in the collection
    create: true
    editor:
      preview: false
    fields:
      - label: 'Title'
        name: 'title'
        required: true
        widget: 'string'
      - label: 'Description'
        name: 'description'
        required: false
        widget: 'string'
      - label: 'Background color'
        name: 'backgroundColor'
        required: true
        widget: 'string'
      - label: 'Text color'
        name: 'textColor'
        required: true
        widget: 'string'
      - default: 0
        hint: 'Order on the homepage'
        label: 'Order'
        min: 0
        name: 'order'
        required: true
        value_type: 'int'
        widget: 'number'

  - label: 'Settings'
    extension: 'json'
    name: 'settings'
    editor:
      preview: false
    files:
      - label: 'Website'
        name: 'website'
        file: 'src/_data/website.json'
        fields:
          - { label: Author, name: author, widget: string }
          - { label: Name, name: name, widget: string }
          - { label: URL, name: url, widget: string }
          - { label: Year, name: year, widget: string }
