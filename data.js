const fs = require('fs').promises;
const path = require('path');
const sortJson = require('sort-json');

const readDirectory = async (directory) => {
  const directoryFiles = await fs.readdir(directory);
  const isJSONFile = (filename) => path.extname(filename) === '.json';
  const getDirectoryPath = (filename) => path.join(directory, filename);
  const jsonFiles = directoryFiles.filter(isJSONFile).map(getDirectoryPath);

  const readFile = async (filePath) => {
    const content = await fs.readFile(filePath, 'utf8');
    const json = JSON.parse(content);
    json.slug = path.parse(filePath).name;
    return json;
  };
  return Promise.all(jsonFiles.map(readFile));
};

const sortJSON = (json) => sortJson(json, {});
const sortOrder = (a, b) => a.order - b.order;
const sortTitle = (a, b) => a.title.localeCompare(b.title);
const sort = (collection) =>
  ({
    series: sortTitle,
    status: sortOrder,
  }[collection]);

const prettyPrint = (json) => JSON.stringify(json, null, 2);

(async () => {
  const directoryCollection = 'data';
  const collections = ['status', 'series'];

  let status, series;

  const readWriteJSONDirectory = async (collection) => {
    let data = await readDirectory(`${directoryCollection}/${collection}`);
    data = data.map(sortJSON);
    data = data.sort(sort(collection));

    if (collection === 'status') {
      status = data;
      return fs.writeFile(`data/${collection}.json`, prettyPrint(data));
    } else {
      series = data;
    }
  };

  // Make sure the data directory exists
  await fs.mkdir('data', { recursive: true });

  await Promise.all(collections.map(readWriteJSONDirectory));

  const statusMap = {};

  const addToStatusMap = (status) => {
    const { title, backgroundColor, textColor } = status;

    statusMap[status.slug] = {
      title,
      backgroundColor,
      textColor,
      slug: status.slug,
    };
  };
  status.forEach(addToStatusMap);

  const setStatus = (serie) => {
    const status = serie.status;
    const currentSeason = serie.currentSeasonNumber;
    const numberOfSeasons = serie.numberOfSeasons;
    const nextSeason = currentSeason + 1;

    let text =
      {
        cancelled: `Cancelled after season ${numberOfSeasons}`,
        finished: `Finished all ${numberOfSeasons} seasons`,
        ready: `Ready for season ${currentSeason}`,
        waiting: `Waiting for season ${nextSeason + (serie.nextSeasonDate ? ` (${serie.nextSeasonDate})` : '')}`,
        wanting: `Wanting to watch the first season`,
        watching: `Watching season ${currentSeason} of ${numberOfSeasons}`,
      }[status] || '';

    if (status === 'watching' && currentSeason === numberOfSeasons) {
      text = `Watching the latest season (${currentSeason})`;
    }

    serie.status = statusMap[status];
    serie.statusText = text;

    return serie;
  };

  return fs.writeFile(`data/series.json`, prettyPrint(series.map(setStatus)));
})();
